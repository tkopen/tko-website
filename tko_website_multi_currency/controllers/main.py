# -*- coding: utf-8 -*-

import json
import simplejson
import odoo
from odoo import http, tools, _
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.web.controllers.main import Home, ensure_db
import requests
from urllib.request import urlopen


class WebsiteSale(WebsiteSale):
    @http.route(['/get_currency_details'], type='json', auth="public", website=True)
    def get_currency_details(self, currency_id=0, **post):
        currency_id = int(json.loads(currency_id))
        if currency_id:
            currency_obj = request.env['res.currency'].sudo().search([('id', '=', currency_id)])
            pricelist_obj = request.env['product.pricelist']
            price_list_id = pricelist_obj.sudo().search(
                [('name', '=', currency_obj.name), ('currency_id', '=', currency_obj.id)], limit= 1)
            if not price_list_id:
                price_list_id = pricelist_obj.sudo().create({
                    'name': currency_obj.name,
                    'currency_id': currency_obj.id
                })
            request.session['website_sale_current_pl'] = False
            request.session['website_sale_current_pl'] = price_list_id.id
            return True
