from odoo import models
from odoo.http import request
import requests

class IrHttp(models.AbstractModel):
    _inherit = ['ir.http']

    @classmethod
    def get_isocountry(cls):
        addr = request.httprequest.headers.environ['REMOTE_ADDR']
        # addr = '43.224.124.0'#'27.131.32.0'#'1.10.0.0'#'5.101.108.0' #'157.230.13.186'

        if addr == '':
            url = 'https://ipinfo.io/json'
        else:
            url = 'https://ipinfo.io/' + addr + '/json'
        res = requests.get(url).json()
        iso_country = res.get('country') or None
        if not iso_country:
            iso_country =  'US'
        request.httprequest.session.geoip['country_code'] = iso_country
        return iso_country

    @classmethod
    def _add_dispatch_parameters(cls, func):
        # only called for is_frontend request
        # set frontend language from IP address
        context = dict(request.context)
        if request.is_frontend:
            if not request.httprequest.session.get('geoip'):
                print ("Resetting geoip.....................................")
                request.httprequest.session['geoip'] = {}
            if not request.httprequest.session.geoip.get('country_code'):
                cls.get_isocountry()
            country_code = request.httprequest.session.geoip.get('country_code')
            country = request.env['res.country'].sudo().search([('code', '=', country_code)], limit=1)
            if len(country):
                if not request.session.get('website_sale_current_pl') and country.pricelist_id and country.pricelist_id.selectable:
                    print (" SETTING PRICE LIST for ......................%s:%s"%(country.name, country.pricelist_id.name))
                    request.session['website_sale_current_pl'] = country.pricelist_id.id
                if not request.httprequest.cookies.get('frontend_lang') and country.lang_id and country.lang_id.active:
                    print(" SETTING LANG for ......................%s:%s" % (country.name, country.lang_id.name))
                    request.lang = context['lang'] = country.lang_id.code # Get the country code here

            # bind modified context
            request.context = context
        return super(IrHttp, cls)._add_dispatch_parameters(func)


