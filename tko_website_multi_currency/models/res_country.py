from odoo import models, fields, api


class ResCountry(models.Model):
    _inherit = 'res.country'

    pricelist_id = fields.Many2one('product.pricelist', 'Pricelist')
    lang_id = fields.Many2one('res.lang', 'Language')
