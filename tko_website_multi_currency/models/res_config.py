# -*- coding: utf-8 -*-


from odoo import fields, models, api
from datetime import datetime


class WebsiteConfig(models.TransientModel):
    _inherit = "res.config.settings"

    is_enable = fields.Boolean(string="Enable Multi Currency")
    country_ids = fields.Many2many('res.country', string="Country")

    @api.model
    def default_get(self, fieldsname):
        res = super(WebsiteConfig, self).default_get(fieldsname)
        record_id = self.search(([]), order='id desc', limit=1)
        if record_id:
            res.update({'is_enable': record_id.is_enable,'country_ids':[(6,0,record_id.country_ids.ids)]})
        return res

    def check_is_enable(self):
        return self.search([], order='id desc', limit=1).is_enable

    def get_country_list(self):
        country_list = self.env['res.country'].search([] ,order='id desc')
        print ("Returning country list............", country_list)
        return country_list

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: