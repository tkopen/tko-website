from odoo import models, fields, api


class ProductPricelist(models.Model):
    _inherit = 'product.pricelist'

    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse')


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    ## SET THE Warehouse and Company from Pricelist
    @api.model
    def create(self, vals):
        if vals.get('pricelist_id') and vals.get('website_id'):
            pricelist = self.env['product.pricelist'].browse(vals['pricelist_id'])
            if pricelist.warehouse_id:
                vals.update({'warehouse_id': pricelist.warehouse_id.id, 'company_id': pricelist.warehouse_id.company_id.id})

        result = super(SaleOrder, self).create(vals)
        return result


    