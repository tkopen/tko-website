from odoo import api, fields, models, tools
from odoo.addons.website.models import ir_http
import requests
import logging

_logger = logging.getLogger(__name__)


class Website(models.Model):
    _inherit = 'website'

    def get_isocountry(self, req):
        addr = req.httprequest.headers.environ['REMOTE_ADDR']
        addr = '1.10.0.0'  # '5.101.108.0' #'157.230.13.186'

        if addr == '':
            url = 'https://ipinfo.io/json'
        else:
            url = 'https://ipinfo.io/' + addr + '/json'
        res = requests.get(url).json()
        iso_country = res.get('country') or None
        if not iso_country:
            iso_country = self.env.user.company_id.country_id.code or 'US'
        req.session.geoip['country_code'] = iso_country
        return iso_country

    def _get_pricelist_available(self, req, show_visible=False):
        """ Return the list of pricelists that can be used on website for the current user.
        Country restrictions will be detected with GeoIP (if installed).
        :param bool show_visible: if True, we don't display pricelist where selectable is False (Eg: Code promo)
        :returns: pricelist recordset
        """
        # super(Website, self)._get_pricelist_available(req, show_visible=show_visible)
        # if not req.session.get('website_sale_current_pl'):

        isocountry = req and req.session.geoip and req.session.geoip.get('country_code') or False
        if not isocountry:
            isocountry = self.get_isocountry(req)
        country = self.env['res.country'].search([('code', '=', isocountry)], limit=1)
        if not req.session.get('website_sale_current_pl'):
            req.session['website_sale_current_pl'] = country.pricelist_id.id
            _logger.info(
                "Setting Pricelist for country %s : %s.............." % (country.name, req.session.geoip.get('country_code')))
        partner = self.env.user.partner_id
        partner_pl = req.session['website_sale_current_pl']
        website = ir_http.get_request_website()
        if not website:
            if self.env.context.get('website_id'):
                website = self.browse(self.env.context['website_id'])
            else:
                # In the weird case we are coming from the backend (https://github.com/odoo/odoo/issues/20245)
                website = len(self) == 1 and self or self.search([], limit=1)
        pricelists = self.env['product.pricelist'].sudo().search(
            [('selectable', '=', True), '|', ('website_id', '=', False), ('website_id', '=', website.id)])
        return pricelists
        # # pricelists = website._get_pl_partner_order(isocountry, show_visible,
        # #                                            website.user_id.sudo().partner_id.property_product_pricelist.id,
        # #                                            req and req.session.get('website_sale_current_pl') or None,
        # #                                            website.pricelist_ids,
        # #                                            partner_pl=partner_pl or None,
        # #                                            order_pl=partner_pl or None)
        # return self.env['product.pricelist'].browse(pricelists)
