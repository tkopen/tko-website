# -*- coding: utf-8 -*-
{
    'name': 'Website Multi Currency and language based on IP Address',
    'summary': 'Module sets currency and language based on IP Address.',
    'version': '1.0',
    'description' :"""Module allow user to select multi currency in website.""",
    'author': 'TKOPEN',
    'category': 'Website',
    'website': "http://www.tkopen.com",
    'currency': 'EUR',
    'depends' : ['base', 'website_sale', 'sale_management', 'website'],
    'data' : [
        'views/templates.xml',
        'views/res_country_view.xml',
        'views/pricelist_view.xml',
    ],
    'images': ['static/description/main_screenshot.png'],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
